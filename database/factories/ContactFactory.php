<?php

use Faker\Generator as Faker;
use App\Contact;

$factory->define(Contact::class, function (Faker $faker) {
    return [
       'name' => $faker->name,
       'lastname' => $faker->lastName,
       'email' => $faker->freeEmail,
       'address'=> $faker->address,
       'phone'=> $faker->buildingNumber       
   ];
});


